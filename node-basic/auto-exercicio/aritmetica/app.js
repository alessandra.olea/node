//const helpers = require ("./calculator");

const {division, multiplication, sum, subtraction} = require ("./calculator");

//this is a core module
const http = require('http');

//this is a core module
const server = http.createServer((req, res) => {
	res.end("This is a C-A-L-C-U-L-A-T-O-R-!--XD");
});

//core module
server.listen(3000);

const totalDivision = division(0,1);
const totalMultiplication = multiplication(0,1);
const totalSum = sum(0,1);
const totalSubtraction = subtraction(0,1);

//print
console.log("Total da divisão: ", totalDivision);
console.log("Total da multiplicação: ", totalMultiplication);
console.log("Total da soma: ", totalSum);
console.log("Total da subtração: ", totalSubtraction);

//input/output
var readline = require('readline');
var resp = "";

var leitor = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log("Considerando as 4 operações fundamentais da aritmética tidas pelos elementos 'A' em relação com 'B'.");

leitor.question("Qual é o valor 'A' obtido através destes resultados?\n", function(answer) {
	var resp = answer;
	
	if (answer == 0){
		console.log("\nSua resposta '" + resp + "' está correta.");
	}
	else{
	console.log("\nSua resposta '" + resp + "' foi gravada com sucesso em uma variável inútil. \nPorém ela está incorreta!");
	};

	leitor.close();
});

//for run the application in the brownser "localhost:3000"