//const helpers = require ("./calculator");

const {division, multiplication, sum, subtraction} = require ("./calculator");

//this is a core module
const http = require('http');

//this is a core module
const server = http.createServer((req, res) => {
	res.end("This is a C-A-L-C-U-L-A-T-O-R-!--XD");
});

//core module
server.listen(3000);

//input/output
var readline = require('readline');
var resp = "";

var leitor = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

console.log("Vamos realizar operações aritméticas!");

leitor.question("Digite um valor para 'A':\n", function(answer) {
	var resp = answer;

	const totalDivision = division(answer,1);
	const totalMultiplication = multiplication(answer,1);
	var totalSum = sum(answer,1);
	const totalSubtraction = subtraction(answer,1);

//print
console.log("Total da divisão: ", totalDivision);
console.log("Total da multiplicação: ", totalMultiplication);
console.log("Total da soma: ", totalSum);
console.log("Total da subtração: ", totalSubtraction);
	
	if (answer >= 0){
		console.log("\nSua resposta '" + resp + "' é um número positivo.");
	}
	else{
	console.log("\nSua resposta '" + resp + "' é um número negativo. \nQue tal continuarmos?");
	};

	leitor.close();
});

//for run the application in the brownser "localhost:3000"